import requests

def send_test(url, text):
    # Open the file in binary mode
 
    response = requests.post(url, data=str({"text": text}))
    return response

# Specify the URL of your Flask app
url = 'http://localhost:6688/analyze_sentiment'

# Send the file and get the response
response = send_test(url, "This is a long sentence, which should be split into multiple parts. Each part might still be too long and could be split further based on the word limit ")

if response.status_code == 200:
    print("Successfully connected to the app. Response:")
    print(response.json())  # Print the JSON response from the server
else:
    print("Failed to connect to the app. Status code:", response.status_code)