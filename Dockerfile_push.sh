#!/bin/bash

# Load environment variables
if [ -f .env ]; then
    export $(cat .env | sed 's/#.*//g' | xargs)
else
    echo "Error: .env file not found."
    exit 1
fi

# Debug: print variables to check
echo "Docker Username: $DOCKER_USERNAME"
echo "Docker Image: $IMAGE_NAME"
echo "Docker Tag: $TAG"

# Check if variables are set
if [ -z "$DOCKER_USERNAME" ] || [ -z "$DOCKER_PASSWORD" ] || [ -z "$IMAGE_NAME" ] || [ -z "$TAG" ]; then
    echo "Error: One or more required variables are undefined."
    exit 1
fi

# Build the Docker image
docker build -t $IMAGE_NAME:$TAG .

# Login to Docker Hub
echo "Logging into Docker Hub"
echo $DOCKER_PASSWORD | docker login --username $DOCKER_USERNAME --password-stdin

# Check if Docker login was successful
if [ $? -ne 0 ]; then
    echo "Docker login failed."
    exit 1
fi

# Push the Docker image to Docker Hub
docker push $IMAGE_NAME:$TAG

if [ $? -eq 0 ]; then
    echo "Build and push completed successfully."
else
    echo "Failed to push the Docker image."
    exit 1
fi
