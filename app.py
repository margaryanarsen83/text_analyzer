from flask import Flask, request, jsonify
import openai
import os 
app = Flask(__name__)

# Load your API key from an environment variable or secure place
api_key = "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"
client = openai.OpenAI( organization='org-qZipkLErGNc5Zs02e9XU4ph3',
  api_key=api_key)


def ask(text):

    completion = client.chat.completions.create(
                            model="gpt-3.5-turbo-16k-0613",
                            messages=[
                                {"role": "system", "content": "You are a helpful assistant.and you will return main topic from given text in this format {'topic': 'your response'}"},
                                {"role": "user", "content": text}
                            ]
                            )

    return completion.choices[0].message

@app.route('/analyze_sentiment', methods=['POST'])
def analyze_sentiment():
    data = request.form.get("text")
  
    response = ask(data)
    print(response)
    return jsonify({'Topic': str(response)})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6687)